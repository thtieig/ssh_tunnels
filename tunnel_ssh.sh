#!/bin/bash
#
# ==================================================== #
# Tunnel from CURRENT_HOST to DEST_HOST via MIDDLE_BOX #
# ==================================================== #
#
# The scripts connects the local port $LOC_PORT 
# to the $DEST_BOX_SSH_PORT on $DEST_BOX_HOST
# via $MIDDLE_BOX_HOST.
#
############################################################
source tunnel_ssh_settings
############################################################

CHECK_TUNS=$(ps aux | grep "[s]sh -N -f -p $MIDDLE_BOX_SSH_PORT -L$LOC_PORT:$DEST_BOX_HOST:$DEST_BOX_SSH_PORT $MIDDLE_BOX_USER@$MIDDLE_BOX_HOST" | awk '{print $2}')

N_TUNS=$(echo $CHECK_TUNS | wc -l)

create_tunnel(){
  # Create a connection between localhost:$LOC_PORT to MIDDLE_BOX:SSH_PORT
  # It will ask for MIDDLE_BOX's password
  # -N -f keep the connection open in background executing No commands
  ssh -N -f -p $MIDDLE_BOX_SSH_PORT -L$LOC_PORT:$DEST_BOX_HOST:$DEST_BOX_SSH_PORT $MIDDLE_BOX_USER@$MIDDLE_BOX_HOST
  echo "Created new tunnel"
}

check_tunnel(){
nc -w 1 -z localhost $LOC_PORT > /dev/null 2>&1
}

reset_tunnel() {
for PID in $CHECK_TUNS; do
   kill -9 $PID > /dev/null 2>&1
   echo "Found multiple tunnels. Killed all."
done
}


# Hidden function. Add 'cleanup' as argument to close all the tunnels
[ "$1" == "cleanup" ] && reset_tunnel && exit 0


if [ $N_TUNS -eq 0 ] ; then
   create_tunnel
elif [ $N_TUNS -eq 1 ] ; then
   check_tunnel
   if [ $? -eq 0 ] ; then
      echo "Tunnel already up and running"
   else
      reset_tunnel
      create_tunnel
   fi
else
   reset_tunnel
   create_tunnel
fi


CHECK_SOCK=$(ps aux | grep -q "[s]sh -D$SOCK_PORT -p$LOC_PORT $DEST_BOX_USER@localhost")
if [ $? -eq 0 ] ; then
   echo "Sock already created on port $SOCK_PORT - just opening SSH shell on $DEST_BOX_HOST"
   ssh -p$LOC_PORT $DEST_BOX_USER@localhost
 else
   # This will open an SSH shell from DEST_BOX *AND* create a sock proxy on port $SOCK_PORT locally
   # You can use FoxyProxy in your browser to browse the DEST_BOX's network
   # Just set "localhost", dest port "$SOCK_PORT" and select "Socks Proxy"
   echo "Created sock on port $SOCK_PORT and ssh'ing on $DEST_BOX_HOST"
   ssh -D$SOCK_PORT -p$LOC_PORT $DEST_BOX_USER@localhost
fi


