#!/bin/bash
#
# ==================================================== #
# Tunnel from CURRENT_HOST to DEST_HOST via MIDDLE_BOX #
# ==================================================== #
#
# The scripts connects the local port $LOC_PORT 
# to $DEST_BOX_RDP_PORT on DEST_BOX via MIDDLE_BOX.
#
source tunnel_RDP_settings

############################################################

CHECK_TUNS=$(ps aux | grep "[s]sh -N -f -p $MIDDLE_BOX_SSH_PORT -L$LOC_PORT:$DEST_BOX_HOST:$DEST_BOX_RDP_PORT $MIDDLE_BOX_USER@$MIDDLE_BOX_HOST" | awk '{print $2}')

N_TUNS=$(echo $CHECK_TUNS | wc -l)

create_tunnel(){
  # Create a connection between localhost:$LOC_PORT to MIDDLE_BOX:SSH_PORT
  # It will ask for MIDDLE_BOX's password
  # -N -f keep the connection open in background executing No commands
  ssh -N -f -p $MIDDLE_BOX_SSH_PORT -L$LOC_PORT:$DEST_BOX_HOST:$DEST_BOX_RDP_PORT $MIDDLE_BOX_USER@$MIDDLE_BOX_HOST
  echo "Tunnel created. Please RDP to localhost:$LOC_PORT and you will get connected to $DEST_BOX_HOST RDP port $DEST_BOX_RDP_PORT."
}

check_tunnel(){
nc -w 1 -z localhost $LOC_PORT > /dev/null 2>&1
}

reset_tunnel() {
for PID in $CHECK_TUNS; do
   kill -9 $PID > /dev/null 2>&1
   echo "Found multiple tunnels. Killed all."
done
}


# Hidden function. Add 'cleanup' as argument to close all the tunnels
[ "$1" == "cleanup" ] && reset_tunnel && exit 0


if [ $N_TUNS -eq 0 ] ; then
   create_tunnel
elif [ $N_TUNS -eq 1 ] ; then
   check_tunnel
   if [ $? -eq 0 ] ; then
      echo "Tunnel already up and running"
   else
      reset_tunnel
      create_tunnel
   fi
else
   reset_tunnel
   create_tunnel
fi

