#!/bin/bash
#
# CLEAN UP ALL TUNNELS

TUNS=$(ps aux | grep "[s]sh.*-L" )
CHECK_TUNS=$(ps aux | grep "[s]sh.*-L" | awk '{print $2}')

echo -e "The following tunnels will be killed.\n$TUNS\nPress any key to continue..."

read voidvar

reset_tunnel() {
for PID in $CHECK_TUNS; do
   kill -9 $PID > /dev/null 2>&1 
   echo "Killed $PID"
done
}

reset_tunnel

