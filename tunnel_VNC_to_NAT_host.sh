#!/bin/bash
#
# ============================================ #
# PORT FORWARD from CURRENT_HOST to DEST_HOST  #
# via LOC_BASTION and REMOTE_BASTION           #
# ============================================ #
#
# The scripts creates an SSH tunnel connecting
# the local port TUN_LOC_PORT to the REMOTE_BASTION ssh port 
# via LOC_BASTION.
# After that, it forwards the remote port DEST_FW_PORT to
# DEST_FW_PORT using the ssh tunnel just created.
#
###########################################################
source tunnel_VNC_to_NAT_host_settings
############################################################

CHECK_TUNS=$(ps aux | grep "[s]sh -N -f -p $LOC_BASTION_SSH_PORT -L$TUN_LOC_PORT:$REMOTE_BASTION_HOST:$REMOTE_BASTION_SSH_PORT $LOC_BASTION_USER@$LOC_BASTION_HOST" | awk '{print $2}')

N_TUNS=$(echo $CHECK_TUNS | wc -l)

create_tunnel(){
  # Create a connection between localhost:$TUN_LOC_PORT to MIDDLE_BOX:SSH_PORT
  # It will ask for MIDDLE_BOX's password
  # -N -f keep the connection open in background executing No commands
  ssh -N -f -p $LOC_BASTION_SSH_PORT -L$TUN_LOC_PORT:$REMOTE_BASTION_HOST:$REMOTE_BASTION_SSH_PORT $LOC_BASTION_USER@$LOC_BASTION_HOST
  echo "Created new tunnel"
}

check_tunnel(){
nc -w 1 -z localhost $TUN_LOC_PORT > /dev/null 2>&1
}

reset_tunnel() {
for PID in $CHECK_TUNS; do
   kill -9 $PID > /dev/null 2>&1
   echo "Found multiple tunnels. Killed all."
done
}


# Hidden function. Add 'cleanup' as argument to close all the tunnels
[ "$1" == "cleanup" ] && reset_tunnel && exit 0

if [ $N_TUNS -eq 0 ] ; then
   create_tunnel
elif [ $N_TUNS -eq 1 ] ; then
   check_tunnel
   if [ $? -eq 0 ] ; then
      echo "Tunnel already up and running"
   else
      reset_tunnel
      create_tunnel
   fi
else
   reset_tunnel
   create_tunnel
fi


CHECK_PORT_FWD=$(ps aux | grep -q "[s]sh -N -f -p $TUN_LOC_PORT -L$LISTENING_LOC_PORT:$DEST_HOST:$DEST_FW_PORT -l $REMOTE_BASTION_USER localhost")
if [ $? -eq 0 ] ; then
   echo "Port forward already created. Remote port $DEST_FW_PORT should be accessible on localhost port $LISTENING_LOC_PORT"
 else
   # This will create 'link' between $DEST_FW_PORT from $DEST_HOST to $TUN_LOC_PORT on localhost
   echo "Creating link between $DEST_FW_PORT to $TUN_LOC_PORT on localhost via $DEST_HOST"
   ssh -N -f -p $TUN_LOC_PORT -L$LISTENING_LOC_PORT:$DEST_HOST:$DEST_FW_PORT -l $REMOTE_BASTION_USER localhost
   echo "You can now access $DEST_FW_PORT listening on $DEST_HOST from localhost on port $LISTENING_LOC_PORT."
fi

# For MAC users
open vnc://localhost:$LISTENING_LOC_PORT

